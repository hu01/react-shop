import React from 'react';
import logo from './logo.svg';
import styles from './styles.module.scss';

export function AtomIcon() {
    return <img src={logo} className={styles.logo} alt="logo" />;
}
