import React from 'react';
import { TopBar } from 'components';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { HomePage, LoginPage } from 'pages';
import { Container, makeStyles, Theme } from '@material-ui/core';
import { AuthContextProvider } from 'services/AuthContext';

const useStyles = makeStyles((theme: Theme) => ({
    main: {
        flexGrow: 1,
        flexShrink: 0
    },
}));

export function App() {

    const classes = useStyles();

    return (
        <AuthContextProvider>
            <Router>
                <header>
                    <TopBar />
                </header>
                <Container maxWidth="lg" className={classes.main}>
                    <Switch>
                        <Route exact path="/">
                            <HomePage />
                        </Route>
                        <Route exact path="/login">
                            <LoginPage />
                        </Route>
                    </Switch>
                </Container>
                <footer>footer</footer>
            </Router>
        </AuthContextProvider>
    );
}
