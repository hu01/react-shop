import React, { createContext, useEffect, useMemo, useState } from 'react';
import { PropsWithChildren } from 'react';
import { noop } from 'lodash';

export type User = null;

interface AuthContextProps {
    readonly user?: User;
}

interface AuthContextType {
    readonly user: User;
    readonly setUser: (user: User) => void;
}

export const AuthContext = createContext<AuthContextType>({
    user: null,
    setUser: noop,
});

export function AuthContextProvider({ children }: PropsWithChildren<AuthContextProps>) {
    const [user, setUser] = useState<User>(null);

    const value = useMemo<AuthContextType>(() => ({ user, setUser }), [user]);

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}
